FROM frolvlad/alpine-glibc:latest

ENV PATH=/usr/local/texlive/bin/x86_64-linux:$PATH

COPY texlive-profile.txt /tmp/

# set up packages
RUN apk add --no-cache \
      wget \
      perl \
      xz \
      && \
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
    mkdir /tmp/install-tl && \
    tar -xzf install-tl-unx.tar.gz -C /tmp/install-tl --strip-components=1 && \
    /tmp/install-tl/install-tl --profile=/tmp/texlive-profile.txt && \
    apk --no-cache del xz && \
    rm install-tl-unx.tar.gz
